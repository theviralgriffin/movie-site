import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'genrefilter'
})

@Injectable()
export class GenreFilterPipe implements PipeTransform {
    transform(items: any[], field: string, value: string): any[] {
    if (value == '') return items;
    return items.filter(it => it[field].indexOf(value) != -1 );
    }
}