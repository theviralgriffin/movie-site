import { Routes } from '@angular/router';

import { AppComponent } from './component/app';
import { MovieComponent }     from './component/movies';
import { MovieInfoComponent } from './component/movie-info';

export const routes: Routes = [
    {
        path: '',
        redirectTo: '/movies',
        pathMatch: 'full'
    },
    {
        path: 'movies',
        component: MovieComponent
    },
    {
        path: 'view/:id',
        component: MovieInfoComponent
    }

]