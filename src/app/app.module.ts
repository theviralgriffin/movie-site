import { NgModule }           from '@angular/core';
import { BrowserModule }      from '@angular/platform-browser';
import { RouterModule}        from '@angular/router';
import { FormsModule }        from '@angular/forms';

import { AppComponent }       from './component/app';
import { MovieComponent }     from './component/movies';
import { MovieInfoComponent } from './component/movie-info';

import { GenreFilterPipe }    from './pipes/genreFilter';

import { MovieService }       from './services/movies';

import { routes }             from './routes';


@NgModule({
  imports:      [ 
      BrowserModule,
      RouterModule.forRoot(routes),
      FormsModule
  ],
  declarations: [ 
      AppComponent,
      MovieComponent,
      MovieInfoComponent,
      GenreFilterPipe
  ],
  providers: [ MovieService ],
  bootstrap:    [ AppComponent ]
})

export class AppModule { }