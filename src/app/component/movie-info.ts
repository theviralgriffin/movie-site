import { Component, 
         Input,
         OnInit }               from '@angular/core';
import { ActivatedRoute, 
         ParamMap,
         Router,
         NavigationEnd }        from '@angular/router';
import { Location }             from '@angular/common';

import 'rxjs/add/operator/switchMap';

import { Movie }                from '../model/movie';
import { MovieService }         from '../services/movies';


@Component({
    selector: 'movie-info',
    templateUrl: './movie-info.html',
    styleUrls: ['./movie-info.css']
})

export class MovieInfoComponent{
    selectedMovie: Movie = {
        id: 0,
        key: "deadpool",
        name: "",
        description: "",
        genres: ["","",""],
        rate: "",
        length: "",
        img: ""
    };
    similarMovies: any;

    constructor(
        private movieService: MovieService,
        private route: ActivatedRoute,
        private location: Location,
        private router: Router
      ) {}

    ngOnInit(): void{
        this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            window.scrollTo(0, 0)
        });
        this.route.paramMap
            .switchMap((params: ParamMap) => this.movieService.getMovie(+params.get('id')))
            .subscribe(movie => this.selectedMovie = movie);

        this.route.paramMap
            .switchMap((params: ParamMap) => this.movieService.getSimilarMovies(+params.get('id')))
            .subscribe(movies => this.similarMovies = movies);
    }

    selectMovie(id : number){
        this.router.navigate(['/view', id]);
    }

    convertToNumber(rating : string){
        return Number(rating);
    }

    formatGenres(genres : string[]){
        return genres.map((genre) => genre.charAt(0).toUpperCase() + genre.substr(1) ).join(', ');
    }

    goToList(): void {
        this.router.navigate(['/movies']);
    }
}