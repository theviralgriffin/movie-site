import { Component } from '@angular/core';
import { MovieComponent } from './movies';


@Component({
  selector: 'my-app',
  template: `<router-outlet></router-outlet>`,
})

export class AppComponent  { 
    
}