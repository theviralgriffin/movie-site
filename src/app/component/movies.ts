import { Component,
        OnInit }            from '@angular/core';
import { Movie }            from '../model/movie';
import { MovieService }     from '../services/movies';

@Component({
    selector: 'movies',
    templateUrl: './movies.html',
    styleUrls: ['./movies.css']
})

export class MovieComponent implements OnInit{
    movieList : Movie[];
    movieListCopy : Movie[];
    movieGenres : string[];
    searchMovie : string;
    selectedGenre : string = '';

    constructor(private movieService: MovieService){
         
    }

    getData(): void{
        this.movieService.retrieveMovies().then(movies => {
            this.movieList = movies;
            this.movieListCopy = movies;
        });

        this.movieService.retrieveGenres().then(genres => this.movieGenres = genres);
    }

    ngOnInit(): void{
        this.getData();
    }

    selectGenre(genreType: string){
        this.selectedGenre = genreType;
    }

    formatGenres(genres : string[]){
        return genres.map((genre) =>  this.formatGenre(genre)).join(', ');
    }

    formatGenre(genre: string){
        return genre.charAt(0).toUpperCase() + genre.substr(1);
    }

    convertToNumber(rating : string){
        return Number(rating);
    }

    filterMovies(value : string){
        if(value){
            this.movieList = this.movieListCopy.filter(item => item.name.toLowerCase().indexOf(value) > -1);
        }else{
            this.movieList = this.movieListCopy.slice(0);
        }
    } 
}